from pointCloud import *
from cyclops import *


import csv # csv parser

import struct

scene = getSceneManager()
scene.addLoader(BinaryPointsLoader())

# Create a directional light                                                        
light1 = Light.create()
light1.setLightType(LightType.Directional)
light1.setLightDirection(Vector3(-1.0, -1.0, -1.0))
light1.setColor(Color(0.5, 0.5, 0.5, 1.0))
light1.setAmbient(Color(0.2, 0.2, 0.2, 1.0))
light1.setEnabled(True)

# Origin marker
origin = SphereShape.create(1, 4)
origin.setPosition(Vector3(0, 0, 0))
origin.setEffect("colored -d white")

pointProgram = ProgramAsset()
pointProgram.name = "points"
pointProgram.vertexShaderName = "pointCloud/shaders/Sphere.vert"
pointProgram.fragmentShaderName = "pointCloud/shaders/Sphere.frag"
pointProgram.geometryShaderName = "pointCloud/shaders/Sphere.geom"
pointProgram.geometryOutVertices = 4
pointProgram.geometryInput = PrimitiveType.Points
pointProgram.geometryOutput = PrimitiveType.TriangleStrip
scene.addProgram(pointProgram)

#-------------------------------------------------------------------------------
# Read telemetry CSV and generate binary file
def generatePoseFile():
	f = open('C:/Workspace/EVL/ENDURANCE/data-2014-8-4/bonney_2009/Dive17/derived_data/Dive17_21Nov09_poseCorrected.0.txt') # input file
	reader = csv.reader(f, delimiter="\t")

	file = open("Dive17_21Nov09_poseCorrected.0.xyzb", 'wb') # output file

	curRow = 0
	for row in reader:
		row = row[0].split(" ")
		# timestamp orientation pose
		# timestamp (s) is time since 00:00, 1970 Jan 01
		# orientation (rad) is [pitch, roll, yaw]
		# pose (m) is [UTM North, UTM East, Depth from 2008 Dec 10 lake level]
		if( curRow > 4 ):
			timestamp = 0;
			fields = [0,0,0,0,0,0,0];
			curField = 0;
			for col in row:
				if( len(col) > 0 ):
					fields[curField] = col;
					curField += 1;
			
			dataBytes = struct.pack('ddddddd', float(fields[0]), float(fields[1]), float(fields[2]), float(fields[3]), float(fields[4]), float(fields[5]), float(fields[6]) )
			file.write(dataBytes)
		curRow += 1
	f.close()
	file.close()
#-------------------------------------------------------------------------------
def loadPointCloud( filename ):
	pointCloudModel = ModelInfo()
	pointCloudModel.name = 'pointCloud'
	pointCloudModel.path = filename
	#pointCloudModel.options = "10000 100:1000000:5 20:100:4 6:20:2 0:5:1"
	pointCloudModel.options = "10000 100:1000000:20 20:100:10 6:20:5 0:5:5"
	#pointCloudModel.options = "10000 0:1000000:1"
	scene.loadModel(pointCloudModel)

	pointCloud = StaticObject.create(pointCloudModel.name)
	
	pointScale = Uniform.create('pointScale', UniformType.Float, 1)
	pointScale.setFloat(0.5)
	# attach shader uniforms
	mat = pointCloud.getMaterial()
	mat.setProgram(pointProgram.name)
	mat.attachUniform(pointScale)

	return pointCloud;
#-------------------------------------------------------------------------------
#generatePoseFile();

#vehiclePointCloud = loadPointCloud( 'C:/Workspace/EVL/ENDURANCE/bonney-dttools/data/bonney/pose09-17.xyzb' )
vehiclePointCloud2 = loadPointCloud( 'C:/Workspace/EVL/ENDURANCE/bonney-dttools/data/bonney/pose09-27.xyzb' )
#divePointCloud = loadPointCloud( 'C:/Workspace/EVL/ENDURANCE/bonney-dttools/data/bonney/bonney-09-dive17.xyzb' )
divePointCloud2 = loadPointCloud( 'C:/Workspace/EVL/ENDURANCE/bonney-dttools/data/bonney/bonney-09-dive27.xyzb' )
