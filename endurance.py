import sys
from math import *
from euclid import *
from omega import *
from cyclops import *
from pointCloud import *
#from measuringTape import *

import csv # csv parser
import time
import struct
from os import listdir

import DivePointCloud
import PlaneRubberbandSelector
import SondeDrops

scene = getSceneManager()
scene.addLoader(TextPointsLoader())
scene.addLoader(BinaryPointsLoader())

setNearFarZ(1, 100000)

#pointCloudPath = "/data/evl/febret/dttools/data-mh/bonney"
#pointCloudPath = "/data/evl/febret/dttools/data-mh-full-raw/bonney"
#pointCloudPath = "D:/Workspace/omegalib/apps/endurance/data"
pointCloudPath = "C:/Workspace/EVL/ENDURANCE/bonney-dttools/data/bonney"
poseDataPath = "C:/Workspace/EVL/ENDURANCE/data-2014-8-4/bonney_2009"
markedPointspath = "C:/Program Files (x86)/OmegaLib/omegalib/endurance/savedPointMarkers/"

generatePosePointCloud = False


currentMission = 2009
currentDive = 17

poseTranslation = [ 1371377, 435670, 0 ]
#------------------------------------------------------------------------------
# models to load
diveNames = {
		#'dive08-05': pointCloudPath + "/bonney-08-dive05",
		#'dive08-07': pointCloudPath + "/bonney-08-dive07",
		#'dive08-08': pointCloudPath + "/bonney-08-dive08",
		#'dive08-09': pointCloudPath + "/bonney-08-dive09",
		#'dive08-10': pointCloudPath + "/bonney-08-dive10",
		#'dive08-12': pointCloudPath + "/bonney-08-dive12",
		#'dive08-13': pointCloudPath + "/bonney-08-dive13",
		#-'dive08-14': pointCloudPath + "/bonney-08-dive14.xyzb",
		#'dive08-15': pointCloudPath + "/bonney-08-dive15",
		#'dive08-16': pointCloudPath + "/bonney-08-dive16",
		#'dive08-17': pointCloudPath + "/bonney-08-dive17",
		#-'dive08-18': pointCloudPath + "/bonney-08-dive18.xyzb",
		
		# 'dive09-06': pointCloudPath + "/bonney-09-dive06",
		 #'dive09-07': pointCloudPath + "/bonney-09-dive07",
		 #'dive09-08': pointCloudPath + "/bonney-09-dive08",
		# 'dive09-09': pointCloudPath + "/bonney-09-dive09",
		# 'dive09-10': pointCloudPath + "/bonney-09-dive10",
		# 'dive09-11': pointCloudPath + "/bonney-09-dive11",
		# 'dive09-12': pointCloudPath + "/bonney-09-dive12",
		#'dive09-13': pointCloudPath + "/bonney-09-dive13",
		'dive09-17': pointCloudPath + "/bonney-09-dive17",
		#'dive09-18': pointCloudPath + "/bonney-09-dive18",
		#'dive09-19': pointCloudPath + "/bonney-09-dive19",
		#'dive09-20': pointCloudPath + "/bonney-09-dive20",
		#'dive09-21': pointCloudPath + "/bonney-09-dive21",
		#'dive09-22': pointCloudPath + "/bonney-09-dive22",
		#'dive09-23': pointCloudPath + "/bonney-09-dive23",
		#'dive09-24': pointCloudPath + "/bonney-09-dive24",
		#'dive09-25': pointCloudPath + "/bonney-09-dive25",
		#'dive09-26': pointCloudPath + "/bonney-09-dive26",
		#'dive09-27': pointCloudPath + "/bonney-09-dive27"}
		'dive09-17-extracted': pointCloudPath + "/bonney-09-dive17-extracted"}
# diveNames = {
		# 'dive09-13-new': pointCloudPath + "/bonney-09-dive13.xyzb",
		# 'dive09-13-new-rt': pointCloudPath + "/bonney-09-dive13-rt.xyzb",
		# 'dive09-13-old': pointCloudPathOld + "/bonney-09-dive13.xyzb"}
poseNames = {
		'pose09-17': poseDataPath + "/Dive17/derived_data/Dive17_21Nov09_poseCorrected.0"}
		#'pose09-27': poseDataPath + "/Dive27/derived_data/Dive27_02Dec09_poseCorrected.0"}
		
diveColors = {
		# 'dive09-13-new-rt': Color('#909000'),
		# 'dive09-13-new': Color('#900000'),
		# 'dive09-13-old': Color('#000090'),
        
		'dive08-05': Color('#900000'),
		'dive08-07': Color('#009000'),
		'dive08-08': Color('#000090'),
		'dive08-09': Color('#909000'),
		'dive08-10': Color('#900090'),
		'dive08-12': Color('#009090'),
		'dive08-13': Color('#904040'),
		'dive08-14': Color('#009040'),
		'dive08-15': Color('#409040'),
		'dive08-16': Color('#404090'),
		'dive08-17': Color('#909040'),
		'dive08-18': Color('#009000'),
		
		'dive09-06': Color('#AFBD16'),
		'dive09-07': Color('#FFBD16'),
		'dive09-08': Color('#FF7F99'),
		'dive09-09': Color('#C09BFF'),
		'dive09-10': Color('#3FFFB5'),
		'dive09-11': Color('#8CFF00'),
		'dive09-12': Color('#AABF0D'),

		'dive09-13': Color('#ff0000'),
		
		'dive09-17': Color('#00ff00'),
		'dive09-17-extracted': Color('#0000ff'),
		
		#'dive09-18': Color('#0000ff'),
		'dive09-19': Color('#ffff00'),
		'dive09-20': Color('#ff00ff'),
		'dive09-21': Color('#00ffff'),
		'dive09-22': Color('#800000'),
		'dive09-23': Color('#008000'),
		'dive09-24': Color('#000080'),
		'dive09-25': Color('#808000'),
		'dive09-26': Color('#800080'),
		'dive09-27': Color('#008080')}
		
lake = SceneNode.create("lake")

dives = []
poseData = {}

pointDecimation = 50

totalPoints = 0
fieldMin = Vector3(sys.float_info.max,sys.float_info.max, sys.float_info.max)
fieldMax = Vector3(-sys.float_info.max,-sys.float_info.max, -sys.float_info.max)

#------------------------------------------------------------------------------
vehicleData = {}
class VehicleData:
	def __init__(self, row):
		self.timestamp = float(row[0]);
		self.x = float(row[4]);
		self.y = float(row[5]);
		self.z = float(row[6]);
		self.rx = float(row[1]);
		self.ry = float(row[2]);
		self.rz = float(row[3]);
#------------------------------------------------------------------------------
pointMarkerAlpha = 1

class PointMarker:
	def __init__(self, pos, scale, dive):
		global pointMarkerAlpha
		
		self.pos = pos;
		self.dive = dive.name;
		
		self.node = SphereShape.create(0.5,1)
		self.node.setScale(Vector3(scale, scale, scale))
		#self.node.setEffect('colored -e yellow')		
		self.node.setEffect('colored -e yellow -t -a')
		self.node.getMaterial().setAlpha(pointMarkerAlpha)
		self.node.setPosition( pos )
	
	def setScale(self, scale):
		self.node.setScale(scale)
#------------------------------------------------------------------------------
vehicleDataCount = 0
def generatePoseFile(path, poseFilename, outputName):
	global vehicleDataCount
	
	#print("generatePoseFile '"+ pointCloudPath + "/"+outputName+".xyzb' from '" + poseFilename+".txt'")
	
	f = open(poseFilename+".txt") # input file
	reader = csv.reader(f, delimiter="\t")
	
	poseFilename = outputName
	if( generatePosePointCloud ):
		file = open(pointCloudPath + "/"+poseFilename+".xyzb", 'wb') # output file

	curRow = 0
	for row in reader:
		row = row[0].split(" ")
		# timestamp orientation pose
		# timestamp (s) is time since 00:00, 1970 Jan 01
		# orientation (rad) is [pitch, roll, yaw]
		# pose (m) is [UTM North, UTM East, Depth from 2008 Dec 10 lake level]
		if( curRow > 4 ):
			timestamp = 0;
			fields = [0,0,0,0,0,0,0];
			curField = 0;
			for col in row:
				if( len(col) > 0 ):
					fields[curField] = col;
					curField += 1;
			
			# shift fields to x, y, z, rx, ry, rz, timestamp
			x =  float(fields[4]) - float(poseTranslation[0])
			y =  float(fields[5]) - float(poseTranslation[1])
			z =  float(fields[6]) - float(poseTranslation[2])
			
			fields[4] = x;
			fields[5] = -z;
			fields[6] = y;
			
			if( generatePosePointCloud ):
				dataBytes = struct.pack('ddddddd', float(fields[4]), float(fields[5]), float(fields[6]), float(fields[1]), float(fields[2]), float(fields[3]), float(fields[0]) )
				file.write(dataBytes)

			vd = VehicleData(fields);
			vehicleData[vehicleDataCount] = vd;
			vehicleDataCount += 1;
		curRow += 1
	f.close()
	
	poseData[outputName] = vehicleData
	
	if( generatePosePointCloud ):
		file.close()
#------------------------------------------------------------------------------
#generatePoseFile(poseDataPath, "Dive17_21Nov09_poseCorrected.0")
for name,model in poseNames.iteritems():
	generatePoseFile(poseDataPath, model, name)
	
#-------------------------------------------------------------------------------
def loadPointCloud( filename ):
	pointCloudModel = ModelInfo()
	pointCloudModel.name = 'pointCloud'
	pointCloudModel.path = filename
	#pointCloudModel.options = "10000 100:1000000:5 20:100:4 6:20:2 0:5:1"
	pointCloudModel.options = "10000 100:1000000:20 20:100:10 6:20:5 0:5:5"
	#pointCloudModel.options = "10000 0:1000000:1"
	scene.loadModel(pointCloudModel)

	pointCloud = StaticObject.create(pointCloudModel.name)
	
	pointProgram = ProgramAsset()
	pointProgram.name = "points2"
	pointProgram.vertexShaderName = "shaders/PointCloud-Sphere.vert"
	pointProgram.fragmentShaderName = "shaders/PointCloud-Sphere.frag"
	pointProgram.geometryShaderName = "shaders/PointCloud-Sphere.geom"
	pointProgram.geometryOutVertices = 4
	pointProgram.geometryInput = PrimitiveType.Points
	pointProgram.geometryOutput = PrimitiveType.TriangleStrip
	scene.addProgram(pointProgram)

	pointScale = Uniform.create('pointScale', UniformType.Float, 1)
	pointScale.setFloat(0.1)

	# attach shader uniforms
	mat = pointCloud.getMaterial()
	mat.setProgram(pointProgram.name)
	#mat.setColor( Color('white'), Color('white') )
	mat.attachUniform(pointScale)

	return pointCloud;
#-------------------------------------------------------------------------------
vehiclePointCloud = loadPointCloud( pointCloudPath+'/pose09-17.xyzb' )
#vehiclePointCloud = loadPointCloud( pointCloudPath+'/pose09-27.xyzb' )

#------------------------------------------------------------------------------
# load dives
for name,model in diveNames.iteritems():
	dive = DivePointCloud.DivePointCloud(lake, name)
	dive.load(model + ".xyzb", pointDecimation)
	dive.diveNode.getMaterial().setColor(diveColors[name], Color('black'))
	dives.append(dive)
	#totalPoints += dive.diveInfo['numPoints']
	# update field bounds
	minr = dive.diveInfo['minR']
	maxr = dive.diveInfo['maxR']
	ming = dive.diveInfo['minG']
	maxg = dive.diveInfo['maxG']
	minb = dive.diveInfo['minB']
	maxb = dive.diveInfo['maxB']
	if(minr < fieldMin.x): fieldMin.x = minr
	if(ming < fieldMin.y): fieldMin.y = ming
	if(minb < fieldMin.z): fieldMin.z = minb	
	if(maxr > fieldMax.x): fieldMax.x = maxr
	if(maxg > fieldMax.y): fieldMax.y = maxg
	if(maxb > fieldMax.z): fieldMax.z = maxb

print("loaded points: " + str(totalPoints))
print("Field minimums: " + str(fieldMin))
print("Field maximums: " + str(fieldMax))

# Setup attribute bounds uniforms
DivePointCloud.minAttrib.setVector3f(fieldMin)
DivePointCloud.maxAttrib.setVector3f(fieldMax)
DivePointCloud.pointScale.setFloat(0.2)

#------------------------------------------------------------------------------
# load additional models
lakeSonarMeshModel = ModelInfo()
lakeSonarMeshModel.name = "lake-sonar-mesh"
#lakeSonarMeshModel.path = "/data/evl/febret/omegalib-old/appData/endurance/bonney-sonde-bathy.obj"
lakeSonarMeshModel.path = "C:/Workspace/EVL/ENDURANCE/data-2014-8-4/bonney_2009/vis/appData/endurance/bonney-sonde-bathy.obj"
#lakeSonarMeshModel.path = "/data/evl/febret/dttools/data-mh/bonney/bonney-hires.ply"
lakeSonarMeshModel.optimize = True
lakeSonarMeshModel.generateNormals = True
lakeSonarMeshModel.normalizeNormals = True
scene.loadModel(lakeSonarMeshModel)

lakeSonarMesh = StaticObject.create("lake-sonar-mesh")
lakeSonarMesh.setEffect("colored -e white -C -t | colored -d black -w -o -1000 -C -t")
lakeSonarMesh.setVisible(False)
lake.addChild(lakeSonarMesh)

SondeDrops.load()
lake.addChild(SondeDrops.sondeDrops)

# Vehicle marker
vehicleMarker = SphereShape.create(1.5, 4)
vehicleMarker.setPosition(Vector3(0, 0, 0))
vehicleMarker.setEffect("colored -d orange")
vehicleMarker2 = SphereShape.create(1.20, 4)
vehicleMarker2.setPosition(Vector3(0, -0.5, 0))
vehicleMarker2.setEffect("colored -d yellow")

vehicleMarkerY = BoxShape.create(0.2, 5, 0.2)
vehicleMarkerY.setPosition(Vector3(0, 2.5, 0))
vehicleMarkerY.setEffect("colored -d green -e blue")
vehicleMarkerZ = BoxShape.create(0.2, 0.2, 5)
vehicleMarkerZ.setPosition(Vector3(0, 0, 2.5))
vehicleMarkerZ.setEffect("colored -d blue -e green")
vehicleMarkerX = BoxShape.create(5, 0.2, 0.2)
vehicleMarkerX.setPosition(Vector3(2.5, 0, 0))
vehicleMarkerX.setEffect("colored -d red -e red")

vehicleMarker.addChild(vehicleMarker2);
vehicleMarker.addChild(vehicleMarkerX);
vehicleMarker.addChild(vehicleMarkerY);
vehicleMarker.addChild(vehicleMarkerZ);

pivot = SceneNode.create('pivot')
pivot.addChild(lake)
#lake.setPosition(-lake.getBoundCenter())
pivot.pitch(radians(90))

# second light
light = Light.create()
light.setColor(Color(0.8, 0.8, 0.1, 1))
light.setAmbient(Color(0.2, 0.2, 0.2, 1))
light.setPosition(Vector3(0, 2, 0))
#light.setAmbient(Color(0.1, 0.1, 0.1, 1))
#light.setLightDirection(Vector3(0, -1, 0))
light.setEnabled(True)

getDefaultCamera().addChild(light)

scene.setBackgroundColor(Color('black'))

globalScale = 0.01
curScale = 0.001


mm = MenuManager.createAndInitialize()
mm.getMainMenu().get3dSettings().enable3d = False;
mm.getMainMenu().getContainer().setPosition( Vector2(0,25) );
mm.getMainMenu().show();
#------------------------------------------------------------------------------
# dive list
divemnu08 = mm.getMainMenu().addSubMenu('dives-08')
divemnu09 = mm.getMainMenu().addSubMenu('dives-09')
for dive in dives:
	if(dive.name.startswith('dive08')):
		divemnu = divemnu08
	else:
		divemnu = divemnu09
	dbtn = divemnu.addButton(dive.name, 'setDiveVisible("' + dive.name + '", %value%)') 
	btn = dbtn.getButton()
	btn.setCheckable(True)
	btn.setChecked(True)
	btn.setImageEnabled(True)
	btn.getImage().setFillColor(diveColors[dive.name])
	btn.getImage().setFillEnabled(True)
	btn.getImage().setSize(Vector2(20,20))
	
def setDiveVisible(name, visible):
	for dive in dives: 
		if(dive.name == name): 
			dive.diveNode.setVisible(visible)

#------------------------------------------------------------------------------
# Sliders set the angle bounds
boundmnu = mm.getMainMenu().addSubMenu("Bounds")
anglelabel = boundmnu.addLabel("Angle")
angleminslider = boundmnu.addSlider(100, "setAngleBounds(%value%, anglemaxslider.getSlider().getValue())")
angleminslider.getSlider().setValue(0)
angleminslider.getWidget().setWidth(200)
anglemaxslider = boundmnu.addSlider(100, "setAngleBounds(angleminslider.getSlider().getValue(), %value%)")
anglemaxslider.getSlider().setValue(100)
anglemaxslider.getWidget().setWidth(200)

rangelabel = boundmnu.addLabel("Range")
rangeminslider = boundmnu.addSlider(100, "setRangeBounds(%value%, rangemaxslider.getSlider().getValue())")
rangeminslider.getSlider().setValue(0)
rangeminslider.getWidget().setWidth(200)
rangemaxslider = boundmnu.addSlider(100, "setRangeBounds(rangeminslider.getSlider().getValue(), %value%)")
rangemaxslider.getSlider().setValue(100)
rangemaxslider.getWidget().setWidth(200)

# Sets the angle bounds
def setAngleBounds(percMin, percMax):
	range = fieldMax.x - fieldMin.x
	min = fieldMin.x + range * percMin / 100
	max = fieldMin.x + range * percMax / 100
	curMinAttrib = DivePointCloud.minAttrib.getVector3f()
	curMaxAttrib = DivePointCloud.maxAttrib.getVector3f()
	curMinAttrib.x = min
	curMaxAttrib.x = max
	DivePointCloud.minAttrib.setVector3f(curMinAttrib)
	DivePointCloud.maxAttrib.setVector3f(curMaxAttrib)
	anglelabel.setText("Angle: %.1f" % min + "   %.1f" % max)
	
# Sets the range bounds
def setRangeBounds(percMin, percMax):
	range = fieldMax.y - fieldMin.y
	min = fieldMin.y + range * percMin / 100
	max = fieldMin.y + range * percMax / 100
	curMinAttrib = DivePointCloud.minAttrib.getVector3f()
	curMaxAttrib = DivePointCloud.maxAttrib.getVector3f()
	curMinAttrib.y = min
	curMaxAttrib.y = max
	DivePointCloud.minAttrib.setVector3f(curMinAttrib)
	DivePointCloud.maxAttrib.setVector3f(curMaxAttrib)
	rangelabel.setText("Range: %.1f" % min + "   %.1f" % max)

# Sets the time bounds
timeForwardPercent = 0;
timeBackPercent = 0;
def setTimeBounds(percMin, percMax):
	global curMaxtimeIndexForward
	global timeBackPercent
	global timeForwardPercent
	global currentTimeIndex
	
	range = fieldMax.z - fieldMin.z
	#min = fieldMin.z + range * percMin / 100
	#max = fieldMin.z + range * percMax / 100
	timeForwardPercent = percMax
	timeBackPercent = percMin
	
	timeIndexForward = (vehicleDataCount-currentTimeIndex-1) * timeForwardPercent / 100
	timeIndexBackward = (currentTimeIndex * timeBackPercent / 100)
	
	curMinAttrib = DivePointCloud.minAttrib.getVector3f()
	curMaxAttrib = DivePointCloud.maxAttrib.getVector3f()
	
	forwardIndex = currentTimeIndex+timeIndexForward
	backIndex = currentTimeIndex-timeIndexBackward
	
	if( backIndex < 0 ):
		backIndex = 0
		
	curMinAttrib.z = vehicleData[backIndex].timestamp;
	curMaxAttrib.z = vehicleData[forwardIndex].timestamp;
	DivePointCloud.minAttrib.setVector3f(curMinAttrib)
	DivePointCloud.maxAttrib.setVector3f(curMaxAttrib)
	
	structTimeCurrent = time.gmtime(vehicleData[currentTimeIndex].timestamp)
	structTimeForward = time.gmtime(vehicleData[forwardIndex].timestamp)
	structTimeBack = time.gmtime(vehicleData[backIndex].timestamp)
	
	timeAheadDiff = time.mktime( structTimeForward ) - time.mktime( structTimeCurrent )
	timeBackDiff = time.mktime( structTimeCurrent ) - time.mktime( structTimeBack )
	
	timelabel2.setText("Time Range (Minutes): Back: %.0f" %(timeBackDiff/60) + " Forward: %.0f" %(timeAheadDiff/60) )
	
	timeInfoLabel.setText(time.strftime("Current: %a, %d %b %Y %H:%M:%S", structTimeCurrent))
	timeMinInfoLabel.setText(time.strftime("Start: %a, %d %b %Y %H:%M:%S", structTimeBack) + " ("+str(timeBackPercent)+"% back)")
	timeMaxInfoLabel.setText(time.strftime("End: %a, %d %b %Y %H:%M:%S", structTimeForward) + " ("+str(timeForwardPercent)+"% ahead)")

def setCurrentTime(percMin, percMax):
	global vehicleMarker
	global currentTimestamp
	global currentTimeIndex
	global timeInfoLabel
	
	global timeBackPercent
	global timeForwardPercent
	
	range = fieldMax.z - fieldMin.z
	cur = fieldMin.z + range * percMin / 100
	
	if( vehicleData[ percMin ] ):
		vd = vehicleData[ percMin ];
		
		timelabel.setText("Time: "+ time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime(vd.timestamp)))
		vehicleMarker.setPosition( vd.x, vd.y, vd.z );
		vehicleMarker.resetOrientation();
		vehicleMarker.pitch( vd.ry )
		vehicleMarker.roll( vd.rx )
		vehicleMarker.yaw( vd.rz )
		
		
		currentTimestamp = vd.timestamp;
		currentTimeIndex = percMin;
		
		timeIndexForward = (vehicleDataCount-currentTimeIndex-1) * timeForwardPercent / 100
		timeIndexBackward = (currentTimeIndex * timeBackPercent / 100)
		
		curMinAttrib = DivePointCloud.minAttrib.getVector3f()
		curMaxAttrib = DivePointCloud.maxAttrib.getVector3f()

		forwardIndex = currentTimeIndex+timeIndexForward
		backIndex = currentTimeIndex-timeIndexBackward
		
		if( backIndex < 0 ):
			backIndex = 0
			
		curMinAttrib.z = vehicleData[backIndex].timestamp;
		curMaxAttrib.z = vehicleData[forwardIndex].timestamp;
		DivePointCloud.minAttrib.setVector3f(curMinAttrib)
		DivePointCloud.maxAttrib.setVector3f(curMaxAttrib)
		
		structTimeCurrent = time.gmtime(vehicleData[currentTimeIndex].timestamp)
		structTimeForward = time.gmtime(vehicleData[forwardIndex].timestamp)
		structTimeBack = time.gmtime(vehicleData[backIndex].timestamp)
		
		timeAheadDiff = time.mktime( structTimeForward ) - time.mktime( structTimeCurrent )
		timeBackDiff = time.mktime( structTimeCurrent ) - time.mktime( structTimeBack )
		
		#timelabel2.setText("Time Range (Minutes): Back: %.0f" %(timeBackDiff/60) + " Forward: %.0f" %(timeAheadDiff/60) )
		
		timeInfoLabel.setText(time.strftime("Current: %a, %d %b %Y %H:%M:%S", structTimeCurrent))
		timeMinInfoLabel.setText(time.strftime("Start: %a, %d %b %Y %H:%M:%S", structTimeBack) + " ("+str(timeBackPercent)+"% back)")
		timeMaxInfoLabel.setText(time.strftime("End: %a, %d %b %Y %H:%M:%S", structTimeForward) + " ("+str(timeForwardPercent)+"% ahead)")
	
# call both functions to reset bounds and bound labels
setAngleBounds(0, 100)
setRangeBounds(0, 100)

def setTimeScale(timeLevel):
	global timeSpeedLabel
	global timeScale

	s = 10 ** (timeLevel - 4)
	timeSpeedLabel.setText("Time scroll Speed: " + str(s) + "x")
	timeScale = s

def setCameraHeight(height):
	global orbitCamHeightOffset

	s = height
	cameraHeightLabel.setText("Camera height offset: " + str(s))
	orbitCamHeightOffset = -s

def setCameraFollow(toggle):
	global enableOrbitCamera

	enableOrbitCamera = toggle

def setTimeRangeColorEnabled(toggle):
	global timeRangeHighlight
	
	timeRangeHighlight = toggle
	if( timeRangeHighlight == True ):
		DivePointCloud.highlightMode = 1
	else:
		DivePointCloud.highlightMode = 0
#------------------------------------------------------------------------------
# other menu items
timeSpeedLabel = mm.getMainMenu().addLabel("sd")
ss = mm.getMainMenu().addSlider(10, "setTimeScale(%value%)")
ss.getSlider().setValue(4)
ss.getWidget().setWidth(200)

timelabel = mm.getMainMenu().addLabel("CurrentTime")
timeslider = mm.getMainMenu().addSlider(vehicleDataCount, "setCurrentTime(%value%, timeslider.getSlider().getValue())")
timeslider.getSlider().setValue(0)
timeslider.getWidget().setWidth(200)

timelabel2 = mm.getMainMenu().addLabel("TimeRange")

#timeColorBtn = mm.getMainMenu().addButton("Highlight points in range", "setTimeRangeColorEnabled(%value%)")
#timeColorBtn.getButton().setCheckable(True)
#timeColorBtn.getButton().setChecked(True)

timeminslider = mm.getMainMenu().addSlider(100, "setTimeBounds(%value%, timemaxslider.getSlider().getValue())")
timeminslider.getSlider().setValue(0)
timeminslider.getWidget().setWidth(200)
timemaxslider = mm.getMainMenu().addSlider(100, "setTimeBounds(timeminslider.getSlider().getValue(), %value%)")
timemaxslider.getSlider().setValue(5)
timemaxslider.getWidget().setWidth(200)

camFollowbtn = mm.getMainMenu().addButton("Follow Camera", "setCameraFollow(%value%)")
camFollowbtn.getButton().setCheckable(True)
camFollowbtn.getButton().setChecked(True)

cameraHeightLabel = mm.getMainMenu().addLabel("sd")
ss = mm.getMainMenu().addSlider(20, "setCameraHeight(%value%)")
ss.getSlider().setValue(0)
ss.getWidget().setWidth(200)


pointsmnu = mm.getMainMenu().addSubMenu("Points")
pointsClean_mnu = mm.getMainMenu().addSubMenu("Point Cleanup")
pointsCleanLoad_mnu = pointsClean_mnu.addSubMenu("Load Point File")
pointsCleanSaveBtn = pointsClean_mnu.addButton("Save Current Points", "savePointMarkers()")

pointCleanAlphaLabel = pointsClean_mnu.addLabel("Point Transparency")
pointCleanAlphaSlider = pointsClean_mnu.addSlider(100, "setPointMarkerAlpha(%value%)")
pointCleanAlphaSlider.getSlider().setValue(100)
pointCleanAlphaSlider.getWidget().setWidth(200)

pointsClean_mnu.addLabel("")
pointsCleanClearBtn = pointsClean_mnu.addButton("Clear Current Points", "clearPointMarkers()")

bathymnu = mm.getMainMenu().addSubMenu("Bathymetry")
mrm = pointsmnu.addSubMenu("Render Mode")

dropbtn = mm.getMainMenu().addButton("Show Sonde Drops", "SondeDrops.sondeDrops.setVisible(%value%); SondeDrops.sondeDrops.setChildrenVisible(%value%)")
dropbtn.getButton().setCheckable(True)
dropbtn.getButton().setChecked(True)

pointSizeLabel = pointsmnu.addLabel("Point Size")
ss = pointsmnu.addSlider(10, "onPointSizeSliderValueChanged(%value%)")
ss.getSlider().setValue(4)
ss.getWidget().setWidth(200)

pointsmnu.addLabel("Point Transparency")

ss = pointsmnu.addSlider(11, "onAlphaSliderValueChanged(%value%)")
ss.getSlider().setValue(10)
ss.getWidget().setWidth(200)

scaleLabel = mm.getMainMenu().addLabel("Y Scale")
ss = mm.getMainMenu().addSlider(10, "onYScaleSliderValueChanged(%value%)")
ss.getSlider().setValue(2)
ss.getWidget().setWidth(200)

sondebtn = bathymnu.addButton("Show Sonde Bathymetry", "lakeSonarMesh.setVisible(%value%)")
sondebtn.getButton().setCheckable(True)
sondebtn.getButton().setChecked(False)

ptx = bathymnu.addButton("Sonde Bathymetry Transparency", "lakeSonarMesh.getMaterial().setTransparent(%value%)")
ptx.getButton().setCheckable(True)
ptx.getButton().setChecked(False)

ss = bathymnu.addSlider(11, "lakeSonarMesh.getMaterial().setAlpha(%value% * 0.1)")
ss.getSlider().setValue(10)
ss.getWidget().setWidth(200)

def toggleMenu():
	if( mm.getMainMenu().isVisible() ):
		mm.getMainMenu().hide()
	else:
		mm.getMainMenu().show()

uim = UiModule.instance()
wf = uim.getWidgetFactory()
mainButton = wf.createButton('mainButton', uim.getUi())
mainButton.setText("Main Menu")
mainButton.setUIEventCommand('toggleMenu()')
mainButton.setStyleValue('fill', 'black')
mainmnu.getContainer().setPosition(Vector2(5, 25))

#mrm.addButton("Normal", "renderModeNormal()")
#mrm.addButton("Simple", "renderModeSimple()")
abtn = mrm.addButton("Additive", "setAdditive(%value%)")
abtn.getButton().setCheckable(True)
mrm.addLabel("------------------")
mrm.addButton("Color By Dive", "colorByDive()")
mrm.addButton("Color By Angle", "colorByAngle()")
mrm.addButton("Color By Range", "colorByRange()")
mrm.addButton("Color By Depth", "colorByDepth()")
mrm.addButton("Color By Time", "colorByTime()")

ctLabel = mm.getMainMenu().addLabel("Contour interval (1 - 10 meters)")
cts = mm.getMainMenu().addSlider(10, "ctint(%value% + 1)")
cts.getSlider().setValue(1)
cts.getWidget().setWidth(200)


def onPointSizeSliderValueChanged(value):
	global pointSize
	size = ((value + 1) ** 2) * 0.01
	DivePointCloud.pointScale.setFloat(size)
	pointSizeLabel.setText( "Point Diameter: " + str(size*2) + " meter(s)" )
	pointSize = size*2
	
def onAlphaSliderValueChanged(value):
	a = value * 0.1
	setDiveAlpha(a)
		
def setDiveAlpha(v):
	for dive in dives:
		dive.diveNode.getMaterial().setAlpha(v)

def setPointMarkerAlpha(v):
	global pointMarkerAlpha
	
	pointMarkerAlpha = v / 100.0
	for marker in pointMarkers:
		marker.node.getMaterial().setAlpha(pointMarkerAlpha)
	
def onYScaleSliderValueChanged(value):
	scale = (value + 1)
	global scaleLabel
	global lake
	scaleLabel.setText("Y Scale:" + str(scale) + "x")
	lake.setScale(Vector3(1, 1, scale))
	
def renderModeNormal():
	for dive in dives:
		dive.diveNode.getMaterial().setProgram("points")
		dive.diveNode.getMaterial().setTransparent(True)

def renderModeSimple():
	for dive in dives:
		dive.diveNode.getMaterial().setProgram("points-simple")
		dive.diveNode.getMaterial().setTransparent(True)
	
def setAdditive(value):
	for dive in dives:
		dive.diveNode.getMaterial().setAdditive(value)
		dive.diveNode.getMaterial().setDepthTestEnabled(not value)
		
def colorByAngle():
	DivePointCloud.w1.setFloat(1)
	DivePointCloud.w2.setFloat(0)
	DivePointCloud.w3.setFloat(0)
	DivePointCloud.w4.setFloat(0)
	DivePointCloud.fieldMin.setFloat(fieldMin.x)
	DivePointCloud.fieldMax.setFloat(fieldMax.x)

def colorByRange():
	DivePointCloud.w1.setFloat(0)
	DivePointCloud.w2.setFloat(1)
	DivePointCloud.w3.setFloat(0)
	DivePointCloud.w4.setFloat(0)
	DivePointCloud.fieldMin.setFloat(fieldMin.y)
	DivePointCloud.fieldMax.setFloat(fieldMax.y)
	
# Requires SphereMultiColor.vert
def colorByDepth():
	DivePointCloud.w1.setFloat(0)
	DivePointCloud.w2.setFloat(0)
	DivePointCloud.w3.setFloat(1)
	DivePointCloud.w4.setFloat(0)
	#DivePointCloud.fieldMin.setFloat(fieldMin.z)
	#DivePointCloud.fieldMax.setFloat(fieldMax.z)
	DivePointCloud.fieldMin.setFloat(0)
	DivePointCloud.fieldMax.setFloat(50)

# Requires SphereDepthColor.vert
def colorByTime():
	DivePointCloud.w1.setFloat(0)
	DivePointCloud.w2.setFloat(0)
	DivePointCloud.w3.setFloat(1)
	DivePointCloud.w4.setFloat(0)
	DivePointCloud.fieldMin.setFloat(fieldMin.z)
	DivePointCloud.fieldMax.setFloat(fieldMax.z)
	#DivePointCloud.fieldMin.setFloat(0)
	#DivePointCloud.fieldMax.setFloat(50)
	
def colorByDive():
	DivePointCloud.w1.setFloat(0)
	DivePointCloud.w2.setFloat(0)
	DivePointCloud.w3.setFloat(0)
	DivePointCloud.w4.setFloat(1)

# utility method: color data using a hard-cut value (no gradient)
def colorcut(val):
	DivePointCloud.fieldMin.setFloat(val)
	DivePointCloud.fieldMax.setFloat(val + 0.01)
	
#queueCommand(':hint displayWand')
#queueCommand(":autonearfar on")
globalScale = 1
colorByAngle()

setNearFarZ(1, 10000)

#------------------------------------------------------------------------------
# Wand ray information
wandRayPos = None
wandRayDir = None

stationId = 1001
if(isMaster()):
	stationFile = open('newStations.csv', 'w')
else:
	stationFile = None
	
def postPoint(p):
	mcm = getMissionControlClient()
	if(mcm != None):
		mcm.postCommand('addpt({0}, {1}, {2})'.format(p.z, p.x, -p.y))

def ctint(int):
	mcm = getMissionControlClient()
	if(mcm != None):
		mcm.postCommand('ctint({0})'.format(int))

#------------------------------------------------------------------------------
currentTimeIndex = 0;
currentTimestamp = 0;
timeScale = 100;

pointerLastX = 0;
pointerLastY = 0;
pointerLeftHeld = False;
lastPointerEvent = None;

pointerRightHeld = False;
pointerPos = Vector2(0,0);
pointerInitPos = Vector2(0,0);
lastScale = 1;
#------------------------------------------------------------------------------
# PointIntersector
pointMarkers = [];

diveFocusPoint = SphereShape.create(0.5,1)
diveFocusPoint.setScale(Vector3(0, 0, 0)) 

def createPointMarker( pos, scale, dive ):
	marker = PointMarker( pos, scale, dive )
	
	pointMarkers.append( marker )
	
	return marker
	
def removePointMarker( marker ):
	marker.setScale(Vector3(0,0,0))
	pointMarkers.remove(marker)
	print "Removed marker"	

def removeLastPointMarker():
	if( len( pointMarkers ) > 0 ):
		marker = pointMarkers.pop();
		marker.setScale(Vector3(0,0,0))
		print "Removed last marker"
	else:
		print "No previous markers to remove"
		
def clearPointMarkers():
	while( len( pointMarkers ) > 0 ):
		removeLastPointMarker()
#------------------------------------------------------------------------------

# Event callback
def handleEvent():
	e = getEvent()
	global globalScale
	global wandRayPos
	global wandRayDir
	
	global stationId
	global stationFile
	
	global orbitCamPitch;
	global orbitCamYaw;
	global orbitCamDistance;
	global orbitCamHeightOffset;
	global enableOrbitCamera;
	global currentTimeIndex;
	global timeScale;
	
	global pointerLastX
	global pointerLastY
	global pointerLeftHeld
	
	global timeForwardPercent
	global timeBackPercent
	
	global pointSize
	global pointerRightHeld
	global pointerInitPos
	global lastScale
	global lastPointerEvent

	global cameraFocus
	global diveFocusPoint
	global vehicleMarker
	
	# save wand ray
	if(e.getServiceType() == ServiceType.Wand):
		r = getRayFromEvent(e)
		wandRayPos = r[1]	
		wandRayDir = r[2]	
		
	if(not e.isProcessed()):
		cam = getDefaultCamera()
		if(e.getServiceType() == ServiceType.Wand and e.getSourceId() == 1):
			light.setPosition(e.getPosition())
		if(e.isButtonDown(EventFlags.ButtonLeft)): 
			cam.setPosition(Vector3(0, -1, 0))
			cam.setYawPitchRoll(Vector3(0, 0, 0))
		#if(e.isButtonDown(EventFlags.ButtonUp)): 
			#if(cam.isControllerEnabled()): globalScale = globalScale * 2.0
			#tapeSetStart()
		if(e.isButtonDown(EventFlags.ButtonDown)): 
			#p = tape.startHandle.getPosition()
			#if(isMaster()):
			#	stationFile.write('PP' + str(stationId) + ", , " + str(round(p.x, 2)) + ", " + str(round(p.z, 2)) + ", " + str(round(-p.y, 2)) + '\n')
			#	print('PP' + str(stationId) + ", , " + str(round(p.x, 2)) + ", " + str(round(p.z, 2)) + ", " + str(round(-p.y, 2)) + '\n')
			stationId += 1
			#postPoint(p)
			#queueCommand(':post addpt({0}, {1}, {2})'.format(p.z, p.x, -p.y))
			#ptid = SondeDrops.numPoints
			#data = ['PP' + str(stationId), '', p.z, p.x, -p.y]
			#SondeDrops.createSondeDrop(data, stationId, False)
			SondeDrops.numPoints += 1
			#if(cam.isControllerEnabled()): globalScale = globalScale / 2.0
	
	if( e.getServiceType() == ServiceType.Keyboard ):
		if( enableOrbitCamera ):
			# Keyboard camera rotation
			if( e.isKeyDown(ord('s')) ): # I
				orbitCamPitch -= 0.1;
			if( e.isKeyDown(ord('w')) ): # K
				orbitCamPitch += 0.1;
				
			if( e.isKeyDown(ord('a')) ): # J
				orbitCamYaw += 0.1;
			if( e.isKeyDown(ord('d')) ): # L
				orbitCamYaw -= 0.1;
				
			if( e.isKeyDown(ord('q')) ): # U
				orbitCamDistance += 1.5;
			if( e.isKeyDown(ord('e')) ): # 0
				orbitCamDistance -= 1.5;
			
			if( e.isKeyDown(ord('r')) ):
				orbitCamHeightOffset += 1;
			if( e.isKeyDown(ord('f')) ):
				orbitCamHeightOffset -= 1;
				
			e.setProcessed(); # Override default keyboard navigation

		# Scroll through time
		if( e.isKeyDown(45) and currentTimeIndex - timeScale > 0 ): # - (minus)
			currentTimeIndex -= timeScale;
		elif( e.isKeyDown(45) ):
			currentTimeIndex = 0;
		if( e.isKeyDown(61) ): # =
			currentTimeIndex += timeScale;

		if( e.isKeyDown(ord('[')) ): 
			if( timeBackPercent > 0 ):
				setTimeBounds( timeBackPercent - 1, timeForwardPercent);
		if( e.isKeyDown(ord(']')) ): 
			if( timeBackPercent < 100 ):
				setTimeBounds( timeBackPercent + 1, timeForwardPercent);
		if( e.isKeyDown(ord(';')) ): 
			if( timeForwardPercent > 0 ):
				setTimeBounds( timeBackPercent, timeForwardPercent - 1);
		if( e.isKeyDown(ord("'")) ): 
			if( timeForwardPercent < 100 ):
				setTimeBounds( timeBackPercent, timeForwardPercent + 1);
		
		if( e.isKeyDown(ord('c')) ):
			r = getRayFromEvent(lastPointerEvent)
			for dive in dives:
				hitData = hitNode(dive.diveNode, r[1], r[2])
			
				if( hitData[0] ):
					diveFocusPoint.setPosition( hitData[1] )
					cameraFocus = diveFocusPoint
					
				else:
					cameraFocus = vehicleMarker
		
		if( e.isKeyDown(26) ): # 26 == ctrl-z
			removeLastPointMarker()
			
		if( e.isKeyDown(32) ): # 32 == space bar
			r = getRayFromEvent(lastPointerEvent)

			# hitNode on pointCloud node with ray start r[1] and end r[2] points
			newMarker = True
			for dive in dives:
				hitData = hitNode(dive.diveNode, r[1], r[2])
			
				if( hitData[0] ):
					
					for marker in pointMarkers:
						markerNode = marker.node;
						markerHit = hitNode(markerNode, r[1], r[2])
						
						lastScale = 1
						if( markerHit[0] ):
							newMarker = False

					if( newMarker ):
						marker = createPointMarker( hitData[1], 1, dive )
						
     		# Remove marker
			if( newMarker == False ):
				for marker in pointMarkers:
					markerNode = marker.node;
					markerHit = hitNode(markerNode, r[1], r[2])

					if( markerHit[0] ):
						removePointMarker( marker )
						
	if(e.getServiceType() == ServiceType.Pointer):
		lastPointerEvent = e
		pointerX = e.getPosition().x
		pointerY = e.getPosition().y
		
		pointerXDelta = pointerX - pointerLastX;
		pointerYDelta = pointerY - pointerLastY;
		
		# PointIntersector - on pointer right click
		if( e.isButtonDown(EventFlags.Right) ):
			pointerRightHeld = True
			pointerInitPos = e.getPosition()
			
			r = getRayFromEvent(e)
			
			# hitNode on pointCloud node with ray start r[1] and end r[2] points
			for dive in dives:
				hitData = hitNode(dive.diveNode, r[1], r[2])
			
				if( hitData[0] ):
					newMarker = True
					for marker in pointMarkers:
						markerNode = marker.node;
						markerHit = hitNode(markerNode, r[1], r[2])
						
						lastScale = 1
						if( markerHit[0] ):
							newMarker = False

					if( newMarker ):
						marker = createPointMarker( hitData[1], 1, dive )
						
		if( e.isButtonUp (EventFlags.Right) ):
			pointerRightHeld = False
						
		if( pointerRightHeld ):
			r = getRayFromEvent(e)
			
			for marker in pointMarkers:
				markerNode = marker.node;
				hitData = hitNode(markerNode, r[1], r[2])
			
				if( hitData[0] ):
					curDist = math.sqrt((markerNode.getPosition().x - pointerLastX)**2 + (markerNode.getPosition().y - pointerLastY)**2)
					initDist = math.sqrt((markerNode.getPosition().x - pointerX)**2 + (markerNode.getPosition().y - pointerY)**2)
					
					moveDist = initDist - curDist
						
					scaleSensitivity = 0.2
					
					markerScale = markerNode.getScale().x + moveDist * scaleSensitivity
					
					if( markerScale < 1 ):
						markerScale = 1
						
					markerNode.setScale(Vector3(markerScale, markerScale, markerScale))
				
		if( enableOrbitCamera ):
			if( e.isButtonDown( EventFlags.Left ) ):
				pointerLeftHeld = True;
			if( e.isButtonUp( EventFlags.Left ) ):
				pointerLeftHeld = False;
				
			if( pointerLeftHeld ):
				orbitCamYaw += pointerXDelta / 100;
				orbitCamPitch += pointerYDelta / 100;
				e.setProcessed(); # Override normal left click rotation
			
		# Mouse wheel zoom (Zoom event, extraData0)
		pointerLastX = pointerX
		pointerLastY = pointerY	
		
	#e.setProcessed();
	
	setCurrentTime( currentTimeIndex, 0 );
setEventFunction(handleEvent)

lastLabelUpdate = 0

cameraFocus = vehicleMarker
#------------------------------------------------------------------------------
def onUpdate(frame, time, dt):
	global curScale
	global globalScale
	global lastLabelUpdate
	global lbl
	global cam
	
	global enableOrbitCamera
	global currentTimeIndex
	global cameraFocus
	
	curScale += (globalScale - curScale) * dt
	if(abs(curScale - globalScale) > 0.001):
		pivot.setScale(Vector3(curScale, curScale, curScale))
	
	if( enableOrbitCamera ):
		setThirdPersonCamera(getDefaultCamera(), cameraFocus.getPosition() );
	
	vehiclePositionInfoLabel.setText("Position UTM North, East ("+str(vehicleData[currentTimeIndex].x+float(poseTranslation[0]))+", "+str(vehicleData[currentTimeIndex].z+float(poseTranslation[1]))+")")
	vehiclePosition2InfoLabel.setText("Depth (%.3f m)" %(-vehicleData[currentTimeIndex].y+float(poseTranslation[2])))
	
	pitchDeg = "%.3f" %(vehicleData[currentTimeIndex].rx * 180/math.pi)
	rollDeg = "%.3f" %(vehicleData[currentTimeIndex].ry * 180/math.pi)
	yawDeg = "%.3f" %(vehicleData[currentTimeIndex].rz * 180/math.pi)
	
	vehicleOrientationInfoLabel.setText("Orientation Pitch,Roll,Yaw ("+pitchDeg + ", " + rollDeg + ", " + yawDeg + ")")
	
	
	mm.getMainMenu().getContainer().setPosition( Vector2(10,25) );
setUpdateFunction(onUpdate)

#scene.setBackgroundColor(Color('#303030'))

#------------------------------------------------------------------------------
#GUI
ui = UiModule.createAndInitialize()
wf = ui.getWidgetFactory()
uiroot = ui.getUi()

guiOriginX = 600;

#diveInfoLabel = wf.createLabel("diveInfoLabel", uiroot,"Mission: "+str(currentMission)+" Dive: "+ str(currentDive))
#diveInfoLabel.setPosition( Vector2( guiOriginX, 0 ) )
#diveInfoLabel.setLayer(WidgetLayer.Front)
#diveInfoLabel.setFont(fontName+" "+str(cursorLabelFontSize))

timeInfoLabel = wf.createLabel("timeInfoLabel", uiroot,"")
timeInfoLabel.setPosition( Vector2( guiOriginX, 16 * 1 ) )
timeInfoLabel.setLayer(WidgetLayer.Front)

timeMinInfoLabel = wf.createLabel("timeInfoLabel", uiroot,"")
timeMinInfoLabel.setPosition( Vector2( guiOriginX, 16 * 2 ) )
timeMinInfoLabel.setLayer(WidgetLayer.Front)

timeMaxInfoLabel = wf.createLabel("timeInfoLabel", uiroot,"")
timeMaxInfoLabel.setPosition( Vector2( guiOriginX, 16 * 3 ) )
timeMaxInfoLabel.setLayer(WidgetLayer.Front)

vehiclePositionInfoLabel = wf.createLabel("vehiclePosition", uiroot,"")
vehiclePositionInfoLabel.setPosition( Vector2( guiOriginX, 16 * 4 ) )
vehiclePositionInfoLabel.setLayer(WidgetLayer.Front)
vehiclePosition2InfoLabel = wf.createLabel("vehiclePosition", uiroot,"")
vehiclePosition2InfoLabel.setPosition( Vector2( guiOriginX, 16 * 5 ) )
vehiclePosition2InfoLabel.setLayer(WidgetLayer.Front)

vehicleOrientationInfoLabel = wf.createLabel("vehicleOrientation", uiroot,"")
vehicleOrientationInfoLabel.setPosition( Vector2( guiOriginX, 16 * 6 ) )
vehicleOrientationInfoLabel.setLayer(WidgetLayer.Front)
#------------------------------------------------------------------------------
# Third Person Camera
orbitCamDistance = -40
orbitCamYaw = 0.74
orbitCamPitch = 3.24
orbitCamHeightOffset = 0;
enableOrbitCamera = True;

def setThirdPersonCamera(cam, focusPos):
	global orbitCamDistance;
	global orbitCamYaw;
	global orbitCamPitch;
	global orbitCamHeightOffset;
	
	# Clamp rotation
	if( orbitCamYaw > 2 * math.pi ):
		orbitCamYaw -= 2 * math.pi;
	if( orbitCamYaw < 0 ):
		orbitCamYaw += 2 + math.pi;
	if( orbitCamPitch > math.pi * 2.5 ):
		orbitCamPitch = math.pi / 2.5;
	if( orbitCamPitch < -math.pi / 2.5 ):
		orbitCamPitch = -math.pi / 2.5;
	
	# Spherical to cartesian coordinate conversion
	camX = orbitCamDistance * math.cos(orbitCamYaw) * math.cos(orbitCamPitch);
	camZ = orbitCamDistance * math.sin(orbitCamYaw) * math.cos(orbitCamPitch);
	camY = orbitCamDistance * math.sin(orbitCamPitch);
	
	cam.setPosition(focusPos.x + camX, focusPos.y + camY, focusPos.z + camZ);
	
	cam.lookAt( Vector3(focusPos.x, focusPos.y+orbitCamHeightOffset, focusPos.z), Vector3(0,1,0) );
	# Look at focus 
	#zaxis = focusPos - cam.getPosition();
	#z.normalize();
	#yaxis = upVector.cross(zaxis);
	#y.normalize();
	#xaxis = zaxis.cross(yaxiz);
	#x.normalize();
	
	#m = [
	#	[yaxis.x, xaxis.x, zaxis.x],
	#	[yaxis.y, xaxis.y, zaxis.y],
	#	[yaxis.z, xaxis.z, zaxis.z],
	#	]
	
	#qw = math.sqrt( 1 + m[2][1] + m[1][1] + m[2][2] ) / 2;
	#w = 4 * qw;
	#qx = (m[2][1] - m[1][2]) / w;
	#qy = (m[0][2] - m[2][0]) / w;
	#qz = (m[1][0] - m[0][1]) / w;
	#cam.setOrientation(Quaternion(qx, qy, qz, qw))
#------------------------------------------------------------------------------
# selection mode
def onSelectionUpdated():
	sp = PlaneRubberbandSelector.startPoint
	ep = PlaneRubberbandSelector.endPoint
	#tape.startHandle.setPosition(sp)
	#tape.endHandle.setPosition(ep)
	DivePointCloud.minBox.setVector3f(Vector3(min(sp.x, ep.x) -100, min(sp.z, ep.z)))
	DivePointCloud.maxBox.setVector3f(Vector3(max(sp.x, ep.x), 0, max(sp.z, ep.z)))
	
def enableSelectionMode():
	PlaneRubberbandSelector.enabled = True
	PlaneRubberbandSelector.plane = Plane(Vector3(0,1,0), 0.0)
	PlaneRubberbandSelector.selectionUpdatedCallback = onSelectionUpdated
	cam = getDefaultCamera()
	cam.setControllerEnabled(False)
	p = lake.getBoundCenter()
	cam.setPosition(p + Vector3(0, lake.getBoundRadius() * 3, 0))
	cam.setPitchYawRoll(Vector3(radians(-90), 0,0)) #radians(-55), 0))
	DivePointCloud.pointScale.setFloat(1)

#------------------------------------------------------------------------------
# setup the measuring tape
#tape = MeasuringTape()
#tapemnu = mm.getMainMenu().addSubMenu('Measuring Tape')
#tapemnu.addButton('Set End', 'tapeSetEnd()')
#tapemnu.addButton('Set Start', 'tapeSetStart()')
#tapemnu.addButton('Reset Bounds', 'resetBounds()')

#def tapeSetStart():
	# Position 2 meters away along wand.
	#tape.startHandle.setPosition(wandRayPos + wandRayDir * 2)
	#tape.startText.setFacingCamera(getDefaultCamera())
	#tape.startText.setColor(Color('black'))
	
#def tapeSetEnd():
	# Position 2 meters away along wand.
	#tape.endHandle.setPosition(wandRayPos + wandRayDir * 2)
	#tape.endText.setFacingCamera(getDefaultCamera())
	#tape.endText.setColor(Color('black'))

def resetBounds():
	DivePointCloud.minBox.setVector3f(Vector3(-1000,-1000,-1000))
	DivePointCloud.maxBox.setVector3f(Vector3(1000,1000,1000))	

def savePointMarkers():
	filename = 'pointMarkers-'+time.strftime("%Y-%m-%d-%H-%M-%S")+'.txt'
	fullFilePath = markedPointspath+filename
	f = open(fullFilePath, 'w')
	for marker in pointMarkers:
		markerNode = marker.node
		f.write( str(markerNode.getPosition().x) + "," + str(markerNode.getPosition().y) + "," + str(markerNode.getPosition().z) + "," + str(markerNode.getScale().x) + "," + str(marker.dive) + "\n")
	f.close()
	
	print str(len(pointMarkers)) + " points saved to " + fullFilePath
	pointsCleanLoad_mnu.addButton(filename, "loadPointMarkers('"+str(filename)+"')")
	
def loadPointMarkers(filename):
	print "Loaded marker file '" + str(filename) + "'" 
	f = open(markedPointspath+"/"+filename) # input file
	reader = csv.reader(f, delimiter="\t")

	curRow = 0
	for row in reader:
		row = row[0].split(",")
		
		x = float( row[0] )
		y = float( row[1] )
		z = float( row[2] )
		scale = float( row[3] )
		dive.name = row[4]
		
		createPointMarker( Vector3(x,y,z), scale, dive )
#------------------------------------------------------------------------------
# hacks for point picking
#tape.body.setVisible(False)

def t():
	c = getDefaultCamera()
	setThirdPersonCamera(c, vehicleMarker.getPosition() )
	#c.setPosition(-139, 22, -18)
	#c.setOrientation(Quaternion(-0.2, 0.05, 0.95, 0.22))
	
t();
SondeDrops.sondeDrops.setChildrenVisible(True)
DivePointCloud.pointScale.setFloat(0.5)
colorByAngle()
getSceneManager().setBackgroundColor(Color('black'))
setTimeBounds(10,5);
setTimeScale(6)
setCameraHeight(0)

pointSize = 0.5
DivePointCloud.pointScale.setFloat(pointSize/2)
pointSizeLabel.setText( "Point Diameter: " + str(pointSize) + " meter(s)" )

for markerFile in os.listdir(markedPointspath):
	mrkBtn = pointsCleanLoad_mnu.addButton(markerFile, "loadPointMarkers('"+str(markerFile)+"')")